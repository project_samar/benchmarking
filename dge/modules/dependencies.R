loadDependenciesData <- function(isQuiet=TRUE){
  # Data manipulation
  library(magrittr, quietly = isQuiet)
  library(tibble, quietly = isQuiet)
  library(dplyr, quietly = isQuiet)
  library(tidyr, quietly = isQuiet)
  library(doParallel, quietly = isQuiet)
  library(here, quietly = isQuiet)
}

loadDependenciesGraph <- function(isQuiet=TRUE){
  #  Graphs
  library(ggplot2, quietly = isQuiet)
  library(grid, quietly = isQuiet)
  library(RColorBrewer, quietly = isQuiet)
  library(cowplot, quietly = isQuiet)
}

loadDependencies <- function(isQuiet = TRUE, isWarn = FALSE){
  loadDependenciesData(isQuiet)
  loadDependenciesGraph(isQuiet)
  
  # DESeq2
  library(DESeq2, quietly = isQuiet, warn.conflicts = isWarn)
  library(tximport, quietly = isQuiet, warn.conflicts = isWarn)

  # Multiprocessing
  library(doParallel, quietly = isQuiet, warn.conflicts = isWarn)

  # # File path helper
  # Internal modules
  loadModules()
  
  # Load options
  loadOptions()
  
  setTimestampNow()
}

loadModules <- function(names=NA){
  DIR_MODULES <- here("modules")    
  
  if(any(!is.na(names))){
    DIR_MODULES <- file.path(DIR_MODULES, names)
  }
  
  moduleFiles <- list.files(path = DIR_MODULES, recursive = TRUE, full.names = TRUE)
  for(m in moduleFiles){source(m)}
}

loadOptions <- function(){
  options(dplyr.summarise.inform = FALSE)
}
