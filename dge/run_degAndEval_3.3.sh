#! /bin/bash

# List of Variables
DIR_COUNTS="data/real_data_drome"
DIR_SIM_REF="data/fruit_fly_data"
DIR_DE_LAST="results/DE_analysis/Real_Data/last"
DIR_DE_RSEM="results/DE_analysis/Real_Data/rsem"
DIR_DE_ASSEMBLY="results/DE_analysis/Real_Data/assembly"
PERC_ALN=0

# List of Constants
FACTORS='["Group"]'
DESIGN="Group"
SAMPLE_INFO='{"sample_1": ["control"], "sample_2": ["control"], "sample_3": ["control"], "sample_4": ["treated"], "sample_5": ["treated"], "sample_6": ["treated"]}'
DE_RES_FILE="Test_result-Group_treated_vs_control.csv"

# DE Analysis - LAST
Rscript modules/DEAnalysis/last2deseq.R \
--in_dir       "${DIR_COUNTS}/last_trained" \
--out_dir      "${DIR_DE_LAST}" \
--factors      "${FACTORS}" \
--design       "${DESIGN}" \
--sample_info  "${SAMPLE_INFO}"

# DE Analysis - RSEM
Rscript modules/DEAnalysis/rsem2deseq.R \
--in_dir       "${DIR_COUNTS}/rsem" \
--file_tx2gene "${DIR_SIM_REF}/genes2transcripts" \
--out_dir      "${DIR_DE_RSEM}" \
--factors      "${FACTORS}" \
--design       "${DESIGN}" \
--sample_info  "${SAMPLE_INFO}"

# DE Analysis - ASSEMBLY
Rscript modules/DEAnalysis/assembly2deseq.R \
--in_dir               "${DIR_COUNTS}/assembly" \
--file_contig2estg     "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
--file_dammitAlignment "${DIR_COUNTS}/assembly/Trinity.fasta.x.A.gambiae.fasta.crbl.bestaln.csv" \
--file_dammitNamemap   "${DIR_COUNTS}/assembly/Trinity.fasta.dammit.namemap.csv" \
--thres_alignedPct     "${PERC_ALN}" \
--out_dir              "${DIR_DE_ASSEMBLY}" \
--factors              "${FACTORS}" \
--design               "${DESIGN}" \
--sample_info          "${SAMPLE_INFO}"

# Evaluation
DIR_GO="data/real_data_drome (GO terms)"

Rscript evaluate_3.3_realData.R \
--out_dir              "results/DE_evaluation/3.3 - Real Data" \
--file_contig2estg     "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
--file_dammitAlignment "${DIR_COUNTS}/assembly/Trinity.fasta.x.A.gambiae.fasta.crbl.bestaln.csv" \
--file_dammitNamemap   "${DIR_COUNTS}/assembly/Trinity.fasta.dammit.namemap.csv" \
--g2t                  "${DIR_SIM_REF}/genes2transcripts" \
--file_fly2FB          "${DIR_SIM_REF}/uniprotkb2flybase(allseqpergene)" \
--file_agam2Fly        "${DIR_SIM_REF}/A.gambiae-D.melanogaster" \
--file_rsemSwiss2Go    "${DIR_GO}/rsemSwiss2Go.csv" \
--file_assemSwiss2Go   "${DIR_GO}/assemSwiss2Go.csv" \
--file_lastSwiss2Go    "${DIR_GO}/lastSwiss2Go.csv" \
--thres_alignedPct      ${PERC_ALN} \
--file_lastRes         "${DIR_DE_LAST}/${DE_RES_FILE}" \
--file_assemblyRes     "${DIR_DE_ASSEMBLY}/${DE_RES_FILE}" \
--file_rsemRes         "${DIR_DE_RSEM}/${DE_RES_FILE}" 