---
title: "3.2 Without organism reference"
---
# DE Analysis code chunk that can be ran in bash 
# These parameters are copied from`run_degAndEval_3.2.sh`
```{r}
# List of Variables
DIR_SIM_DATA="data/fruit_fly_sim_results/20mil_1txperGene_fpkmemtab6584_noref"
DIR_DE_LAST_AGAM="results/DE_analysis/our_agam"
DIR_DE_ASSEM_AGAM="results/DE_analysis/assembly_agam"
DIR_DE_LAST_ANA="results/DE_analysis/our_ananassae"
DIR_DE_ASSEM_ANA="results/DE_analysis/assembly_ananassae"
DIR_DE_LAST_GRIM="results/DE_analysis/our_grimshawi"
DIR_DE_ASSEM_GRIM="results/DE_analysis/assembly_grimshawi"
PERC_ALN=0.5

# List of Constants
FACTORS='["Group"]'
DESIGN="Group"
SAMPLE_INFO='{"sample_1": ["control"], "sample_2": ["control"], "sample_3": ["control"], "sample_4": ["treated"], "sample_5": ["treated"], "sample_6": ["treated"]}'
DE_RES_FILE="Test_result-Group_treated_vs_control.csv"
```

```{r DE Analysis}
source("modules/DEAnalysis/last2deseq.R")
source("modules/DEAnalysis/assembly2deseq.R")

# ----------  A.gambiae  ---------- # 
# DE Analysis - LAST
last2deseq(
  in_dir      = file.path(DIR_SIM_DATA, "our_agambiae_trained_disjoint0.1"),
  out_dir     = DIR_DE_LAST_AGAM,
  factors     = FACTORS,
  design      = DESIGN,
  sample_info = SAMPLE_INFO
)

# DE Analysis - Assembly  
assembly2deseq(
  in_dir               = file.path(DIR_SIM_DATA, "assembly"),
  file_dammitAlignment = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.x.A.gambiae.fasta.crbl.csv"),
  file_dammitNamemap   = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.dammit.namemap.csv"),
  file_contig2estg     = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.gene_trans_map"),
  out_dir              = DIR_DE_ASSEM_AGAM,
  thres_alignedPct     = PERC_ALN,
  factors              = FACTORS,
  design               = DESIGN,
  sample_info          = SAMPLE_INFO
)

# ----------  Dro. Ananassae  ---------- # 
# DE Analysis - LAST
last2deseq(
  in_dir      = file.path(DIR_SIM_DATA, "our_droananassae"),
  out_dir     = DIR_DE_LAST_ANA,
  factors     = FACTORS,
  design      = DESIGN,
  sample_info = SAMPLE_INFO
)

# DE Analysis - Assembly  
assembly2deseq(
  in_dir               = file.path(DIR_SIM_DATA, "assembly"),
  file_dammitAlignment = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.x.dro_ananassae.nodup.fa.crbl.csv"),
  file_dammitNamemap   = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.dammit.namemap.csv"),
  file_contig2estg     = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.gene_trans_map"),
  out_dir              = DIR_DE_ASSEM_ANA,
  thres_alignedPct     = PERC_ALN,
  factors              = FACTORS,
  design               = DESIGN,
  sample_info          = SAMPLE_INFO
)

# ----------  Dro. Grimshawi  ---------- # 
# DE Analysis - LAST
last2deseq(
  in_dir      = file.path(DIR_SIM_DATA, "our_drogrimshawi"),
  out_dir     = DIR_DE_LAST_GRIM,
  factors     = FACTORS,
  design      = DESIGN,
  sample_info = SAMPLE_INFO
)

# DE Analysis - Assembly
assembly2deseq(
  in_dir               = file.path(DIR_SIM_DATA, "assembly"),
  file_dammitAlignment = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.x.dro_grimshawi.nodup.fa.crbl.csv"),
  file_dammitNamemap   = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.dammit.namemap.csv"),
  file_contig2estg     = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.gene_trans_map"),
  out_dir              = DIR_DE_ASSEM_GRIM,
  thres_alignedPct     = PERC_ALN,
  factors              = FACTORS,
  design               = DESIGN,
  sample_info          = SAMPLE_INFO
)
```

# Prepare parameters to run succeeding evaluation code chunks
# These parameters are copied from`run_degAndEval_3.2.sh`
```{r}
DIR_SIM_REF="data/fruit_fly_data"
DIR_SIM_GROUNDTRUTH="data/fruit_fly_sim_results/20mil_1txperGene_fpkmemtab6584_protCoding_rep3_nodup/ground_truth"

g2t                  = file.path(DIR_SIM_REF, "genes2transcripts")
t2p                  = file.path(DIR_SIM_REF, "transcripts2proteins")
file_all_tx_ref      = file.path(DIR_SIM_REF, "transcripts_in_reference")
file_agam2FlyProt    = file.path(DIR_SIM_REF, "A.gambiae-D.melanogaster")
file_ana2FlyProt     = file.path(DIR_SIM_REF, "D.ananassae-D.melanogaster")
file_grim2FlyProt    = file.path(DIR_SIM_REF, "D.grimshawi-D.melanogaster")
file_flyProt2g       = file.path(DIR_SIM_REF, "uniprotkb2flybase(allseqpergene)")
sim_rep_info         = file.path(DIR_SIM_GROUNDTRUTH, "sim_rep_info.txt")
sim_tx_info          = file.path(DIR_SIM_GROUNDTRUTH, "sim_tx_info.txt")
file_contig2estg     = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.gene_trans_map")
file_dammit_agamAlignment = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.x.A.gambiae.fasta.crbl.csv")
file_dammit_anaAlignment  = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.x.dro_ananassae.nodup.fa.crbl.csv")
file_dammit_grimAlignment = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.x.dro_grimshawi.nodup.fa.crbl.csv")
file_dammitNamemap    = file.path(DIR_SIM_DATA, "assembly/Trinity.fasta.dammit.namemap.csv")
file_last_agamRes     = file.path(DIR_DE_LAST_AGAM, DE_RES_FILE)
file_assembly_agamRes = file.path(DIR_DE_ASSEM_AGAM, DE_RES_FILE)
file_last_anaRes      = file.path(DIR_DE_LAST_ANA, DE_RES_FILE)
file_assembly_anaRes  = file.path(DIR_DE_ASSEM_ANA, DE_RES_FILE)
file_last_grimRes     = file.path(DIR_DE_LAST_GRIM, DE_RES_FILE)
file_assembly_grimRes = file.path(DIR_DE_ASSEM_GRIM, DE_RES_FILE)
out_dir              = "results/DE_evaluation/3.2 - No Reference/agam_ana_grim"
thres_alignedPct     = PERC_ALN
testing              = FALSE

# After `attach(opt)`
cutoffs <- if(testing) c(0.01, 0.05, 0.1) else NA
```


# The succeeding code chunks are copied from the Script in `evaluate_3.2_withoutReference.R`

# SECTION 1 - Data Preparation
```{r Data Preparation, echo=FALSE, message=FALSE, warning=FALSE}
source("modules/dependencies.R")
loadDependenciesData()
loadDependenciesGraph()
loadModules()

df_genes <- merge(read.delim(g2t, stringsAsFactors = FALSE), 
                  read.delim(t2p, stringsAsFactors = FALSE), all=TRUE) %>% 
  distinct() %>% 
  mutate(id=paste0(Gene.stable.ID, "|", Transcript.stable.ID),
         UniParc.ID = ifelse(UniParc.ID=="", NA, UniParc.ID)) %>% 
  merge(read.delim(sim_tx_info, stringsAsFactors = FALSE), by.x="id", by.y="transcriptid", all=TRUE) %>% 
  mutate_at(vars(foldchange.1, foldchange.2), ~replace_na(., 1)) %>% 
  mutate_at(vars(DEstatus.1, DEstatus.2), ~replace_na(., "FALSE")) %>% 
  mutate(
    trueLFC = log2(foldchange.2/foldchange.1),
    DEstatus = as.logical(DEstatus.1) | as.logical(DEstatus.2))

df_groundtDEG <- df_genes %>% group_by(Gene.stable.ID) %>% 
  summarise(Transcript.stable.ID = paste0(Transcript.stable.ID, collapse = "|"),
            UniParc.ID = paste0(unique(UniParc.ID), collapse = "|"),
            trueLFCs = paste0(trueLFC, collapse = "|"),
            trueLFC = ifelse(any(trueLFC != 0), trueLFC[which(trueLFC != 0)], 0),
            DEstatuses = paste0(DEstatus, collapse = "|"),
            DEstatus = any(DEstatus),
            trueLfcDir = case_when(trueLFC > 0 ~ "up",
                                   trueLFC < 0 ~ "down",
                                   trueLFC == 0 ~ "none"))

# 03/10/2021 - Update me when changing code below
# Mapping DE Trinity Genes -> SwissProt ID
df_contig2gene <- read.delim(file_contig2estg, header = FALSE, col.names = c("TrinityGene.ID", "Contig.ID")) %>% 
  relocate(Contig.ID)

df_dammit_agamAlignment <- read.csv(file_dammit_agamAlignment) %>% 
  mutate(alignedPct = q_aln_len/q_len) %>% 
  filter(alignedPct >= thres_alignedPct) %>% 
  group_by(q_name) %>% 
  slice_min(E)
df_dammit_anaAlignment <- read.csv(file_dammit_anaAlignment) %>% 
  mutate(alignedPct = q_aln_len/q_len) %>% 
  filter(alignedPct >= thres_alignedPct) %>% 
  group_by(q_name) %>% 
  slice_min(E)
df_dammit_grimAlignment <- read.csv(file_dammit_grimAlignment) %>% 
  mutate(alignedPct = q_aln_len/q_len) %>% 
  filter(alignedPct >= thres_alignedPct) %>% 
  group_by(q_name) %>% 
  slice_min(E)

df_dammitNamemap <- read.csv(file_dammitNamemap) %>% 
  separate(original, sep=" ", into="Contig.ID", extra="drop")

df_tgene2agam <- merge(df_dammitNamemap, df_dammit_agamAlignment, by.x ="renamed", by.y = "q_name") %>% 
  merge(df_contig2gene, by = "Contig.ID") %>% 
  separate(s_name, sep=" ", into="Swissprot.ID", extra="drop") %>% 
  select(TrinityGene.ID, Swissprot.ID) %>% 
  distinct()
df_tgene2ana <- merge(df_dammitNamemap, df_dammit_anaAlignment, by.x ="renamed", by.y = "q_name") %>% 
  merge(df_contig2gene, by = "Contig.ID") %>% 
  separate(s_name, sep=" ", into="Swissprot.ID", extra="drop") %>% 
  select(TrinityGene.ID, Swissprot.ID) %>% 
  distinct()
df_tgene2grim <- merge(df_dammitNamemap, df_dammit_grimAlignment, by.x ="renamed", by.y = "q_name") %>% 
  merge(df_contig2gene, by = "Contig.ID") %>% 
  separate(s_name, sep=" ", into="Swissprot.ID", extra="drop") %>% 
  select(TrinityGene.ID, Swissprot.ID) %>% 
  distinct()

# Extract the middle value in column `Swissprot.ID` from "tr|F5HL97|F5HL97_ANOGA" to column `A.gambiae` with value "F5HL97"
df_tgene2agam <- df_tgene2agam %>% 
                    separate(Swissprot.ID, sep = "\\|", into=c("V1", "A.gambiae", "V2")) %>% 
                    select(-all_of(c("V1", "V2")))
df_tgene2ana <- df_tgene2ana %>% 
                    separate(Swissprot.ID, sep = "\\|", into=c("V1", "D.ananassae", "V2")) %>% 
                    select(-all_of(c("V1", "V2")))
df_tgene2grim <- df_tgene2grim %>% 
                    separate(Swissprot.ID, sep = "\\|", into=c("V1", "D.grimshawi", "V2")) %>% 
                    select(-all_of(c("V1", "V2")))

included_refGenes <- read.delim(file_all_tx_ref, header = FALSE, col.names = "id") %>%
  mutate(id = gsub(">", "", id)) %>%
  separate(id, sep = "\\|", into = c("Gene.stable.ID", "Transcript.stable.ID")) %>%
  distinct() %>% pull(Gene.stable.ID)

df_flyProt2g <- read.delim(file_flyProt2g, header = FALSE, col.names = c("D.melanogaster", "Gene.stable.ID")) %>% 
  filter(Gene.stable.ID %in% included_refGenes) %>%
  merge(select(df_groundtDEG, Gene.stable.ID, DEstatus, trueLFC, trueLfcDir))

df_agam2flyProt <- read.delim(file_agam2FlyProt)
df_ana2flyProt <- read.delim(file_ana2FlyProt)
df_grim2flyProt <- read.delim(file_grim2FlyProt)
df_agam2g <- merge(df_agam2flyProt, df_flyProt2g, by="D.melanogaster", all.x=TRUE)
df_ana2g <- merge(df_ana2flyProt, df_flyProt2g, by="D.melanogaster", all.x=TRUE)
df_grim2g <- merge(df_grim2flyProt, df_flyProt2g, by="D.melanogaster", all.x=TRUE)
df_assg2g_agam <- merge(df_tgene2agam, df_agam2g, all.x=TRUE)
df_assg2g_ana <- merge(df_tgene2ana, df_ana2g, all.x=TRUE)
df_assg2g_grim <- merge(df_tgene2grim, df_grim2g, all.x=TRUE)
```

# SECTION 2 - Read DEG results csv from assembly2deseq & last2deseq
```{r Read DEG results csv from assembly2deseq & last2deseq}
df_assem_agamRes <- read.csv(file_assembly_agamRes, row.names = 1, stringsAsFactors = FALSE) %>% rownames_to_column("genes")
df_last_agamRes <- read.csv(file_last_agamRes, row.names = 1, stringsAsFactors = FALSE) %>% rownames_to_column("genes") %>% 
              separate(genes, sep = "\\|", into = c("V1", "genes", "V2"))
df_assem_anaRes <- read.csv(file_assembly_anaRes, row.names = 1, stringsAsFactors = FALSE) %>% rownames_to_column("genes")
df_last_anaRes <- read.csv(file_last_anaRes, row.names = 1, stringsAsFactors = FALSE) %>% rownames_to_column("genes") %>% 
              separate(genes, sep = "\\|", into = c("V1", "genes", "V2"))
df_assem_grimRes <- read.csv(file_assembly_grimRes, row.names = 1, stringsAsFactors = FALSE) %>% rownames_to_column("genes")
df_last_grimRes <- read.csv(file_last_grimRes, row.names = 1, stringsAsFactors = FALSE) %>% rownames_to_column("genes") %>% 
              separate(genes, sep = "\\|", into = c("V1", "genes", "V2"))

# Map fitted models to ground truth
methods <- list("assem_agam" = list(res = df_assem_agamRes, gtMapping = df_assg2g_agam,  mapToColumn = "TrinityGene.ID", 
                                   isAnyDEGIfMixed = TRUE, isFPIfNAGene = FALSE),
                "last_agam"  = list(res = df_last_agamRes,  gtMapping = df_agam2g,  mapToColumn = "A.gambiae", 
                                   isAnyDEGIfMixed = TRUE, isFPIfNAGene = FALSE),
                "assem_ana" = list(res = df_assem_anaRes, gtMapping = df_assg2g_ana,  mapToColumn = "TrinityGene.ID", 
                                   isAnyDEGIfMixed = TRUE, isFPIfNAGene = FALSE),
                "last_ana"  = list(res = df_last_anaRes,  gtMapping = df_ana2g,  mapToColumn = "D.ananassae", 
                                   isAnyDEGIfMixed = TRUE, isFPIfNAGene = FALSE),
                "assem_grim" = list(res = df_assem_grimRes, gtMapping = df_assg2g_grim,  mapToColumn = "TrinityGene.ID", 
                                   isAnyDEGIfMixed = TRUE, isFPIfNAGene = FALSE),
                "last_grim"  = list(res = df_last_grimRes,  gtMapping = df_grim2g,  mapToColumn = "D.grimshawi", 
                                   isAnyDEGIfMixed = TRUE, isFPIfNAGene = FALSE))
```

# SECTION 3 - Plot PR curve
```{r Plot PR curve}
library("wesanderson")
df_PRcurve <- getDf_curveData(df_groundtDEG, methods, cutoffs)
# p_prCurve <- plotCurves(df_PRcurve)

df_PRcurve <- df_PRcurve %>% separate(method, sep = "_", into=c("method","reference"))

p_prCurve <- ggplot(df_PRcurve, aes(x = sensitivity, y = precision, col = method)) +
  geom_line(lwd=1.3, aes(linetype=reference)) +
  geom_point(data = filter(df_PRcurve, round(pvalcutoff,3) %in% c(0.010, 0.050, 0.100)), 
             shape = 21, stroke=1.1, size = 2.7, fill="white") +
  labs(title="", y="Precision", x="Recall") +
  coord_cartesian(xlim = c(min(df_PRcurve$sensitivity), max(df_PRcurve$sensitivity)),
                  ylim = c(min(df_PRcurve$precision), max(df_PRcurve$precision))) +
  scale_color_manual(values=wes_palette(name="Darjeeling1"),labels=c("assem" = "assembly-based", "last" = "our method")) +
  #scale_color_discrete(labels=c("assem" = "assembly-based", "last" = "our method")) +
  scale_linetype_manual(values=c("dotted", "twodash", "solid"), labels=c("A. gambiae", "D. ananassae","D. grimshawi"))+
  guides(linetype = guide_legend(order=1), color = guide_legend(order=2)) +
  theme_bw() + 
  theme(legend.position = "bottom", 
        legend.box = "vertical",
        legend.title = element_blank(),
        plot.title = element_blank())
p_prCurve
savePlot(p_prCurve, width = 6, height = 5, filedir = out_dir, filename = "PR Curve")
```

# (Optional) Save binary classification results
```{r (Optional) Save binary classification results}
saveCSV(df_PRcurve, filedir = out_dir, filename = "PR Curve (table)")

# # LAST Ground Truth vs Prediction
# df_pred2gt <- merge_pred2Gt(methods[['last']])
# df_class <- getDf_Classification(df_pred2gt, methods[['last']], cutoff = 0.1, isAnyDEGIfMixed = TRUE, isFPIfNAGene = FALSE)
# saveCSV(df_class, filedir = out_dir, filename = "Classification Results - LAST (cutoff 0.1)")
# 
# # Assembly Ground Truth vs Prediction
# df_pred2gt <- merge_pred2Gt(methods[['assem']])
# df_class <- getDf_Classification(df_pred2gt, methods[['assem']], cutoff = 0.1, isAnyDEGIfMixed = TRUE, isFPIfNAGene = FALSE)
# saveCSV(df_class, filedir = out_dir, filename = "Classification Results - Assembly (cutoff 0.1)")
```

# SECTION 4 - Plot LFC boxplot
```{r Plot LFC boxplot}
df_lfcPlot <- getDf_lfcData(methods)

p_lfcPlot <- df_lfcPlot %>% filter(grepl(method,pattern =  "agam")) %>%  plotLFCboxplot(.) +
  scale_color_discrete(labels=c("assem_agam" = "assembly-based", "last_agam" = "our method"))
savePlot(p_lfcPlot, width = 7, height = 5, filedir = out_dir, filename = "LFC Boxplot (agam)")

p_lfcPlot <- df_lfcPlot %>% filter(grepl(method,pattern =  "ana")) %>%  plotLFCboxplot(.) +
  scale_color_discrete(labels=c("assem_ana" = "assembly-based", "last_ana" = "our method"))
savePlot(p_lfcPlot, width = 7, height = 5, filedir = out_dir, filename = "LFC Boxplot (ana)")

p_lfcPlot <- df_lfcPlot %>% filter(grepl(method,pattern =  "grim")) %>%  plotLFCboxplot(.) +
  scale_color_discrete(labels=c("assem_grim" = "assembly-based", "last_grim" = "our method"))
savePlot(p_lfcPlot, width = 7, height = 5, filedir = out_dir, filename = "LFC Boxplot (grim)")
```

# SECTION 5 - Combine figures for publication
```{r}
p_grid <- plot_grid(p_prCurve + theme(plot.title = element_blank()), NULL, p_lfcPlot,
                    rel_widths = c(0.45, 0.05, 0.50), nrow = 1)
p_grid
savePlot(p_grid, width = 9, height = 5, filedir = out_dir, filename = "For publication")
```

