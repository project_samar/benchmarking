#! /bin/bash

# List of Variables
SITE="BAT_cluster70"
DIR_COUNTS="data/mudcrab_revisited/${SITE}"
DIR_DE_LAST="results/DE_analysis/mudcrab_revisited/${SITE}/last"
DIR_DE_ASSEMBLY="results/DE_analysis/mudcrab_revisited/${SITE}/assembly.aln50"
FILE_DAMMIT="${DIR_COUNTS}/assembly/Trinity.fasta.x.sprot_arthropoda_cluster70.fasta.crbl.csv"
FILE_BLAST="${DIR_COUNTS}/assembly/blast.outfmt6"
PERC_ALN=0.5
PVAL=0.01

# List of Constants
FACTORS='["Group"]'
DESIGN="Group"
DE_RES_FILE="Test_result-Group_treated_vs_control.csv"
SAMPLE_INFO='{"sample_1": ["control"], "sample_2": ["control"], "sample_3": ["control"], "sample_4": ["treated"], "sample_5": ["treated"], "sample_6": ["treated"]}'

# DE Analysis - LAST
# Rscript modules/DEAnalysis/last2deseq.R \
# --in_dir       "${DIR_COUNTS}/last" \
# --out_dir      "${DIR_DE_LAST}" \
# --factors      "${FACTORS}" \
# --design       "${DESIGN}" \
# --sample_info  "${SAMPLE_INFO}"

# DE Analysis - Assembly (using Dammit)
# Rscript modules/DEAnalysis/assembly2deseq.R \
# --in_dir               "${DIR_COUNTS}/assembly" \
# --file_contig2estg     "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
# --file_dammitNamemap   "${DIR_COUNTS}/assembly/Trinity.fasta.dammit.namemap.csv" \
# --file_dammitAlignment "${FILE_DAMMIT}" \
# --count_method         "rsem" \
# --out_dir              "${DIR_DE_ASSEMBLY}" \
# --thres_alignedPct     "${PERC_ALN}" \
# --factors              "${FACTORS}" \
# --design               "${DESIGN}" \
# --sample_info          "${SAMPLE_INFO}"

# DE Analysis - Assembly (using BLAST) w/ 0 PERC_ALN
# Rscript modules/DEAnalysis/assembly2deseq.R \
# --in_dir               "${DIR_COUNTS}/assembly" \
# --file_contig2estg     "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
# --file_blastAlignment  "${FILE_BLAST}" \
# --count_method         "rsem" \
# --out_dir              "results/DE_analysis/mudcrab_revisited/${SITE}/assembly.blast" \
# --thres_alignedPct     0 \
# --factors              "${FACTORS}" \
# --design               "${DESIGN}" \
# --sample_info          "${SAMPLE_INFO}"


# Evaluation output
DIR_OUT="results/DE_evaluation/3.x - Mudcrabs/${SITE}_setDiffEval"

# LAST vs Assembly
Rscript evaluate_3.x_mudcrabData.R \
--out_dir 				      "${DIR_OUT}/last_vs_assembly.aln50" \
--file_contig2estg  	  "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
--file_dammitNamemap 	  "${DIR_COUNTS}/assembly/Trinity.fasta.dammit.namemap.csv" \
--file_dammitAlignment  "${FILE_DAMMIT}" \
--file_lastRes 			    "${DIR_DE_LAST}/${DE_RES_FILE}" \
--file_assemblyRes 		  "${DIR_DE_ASSEMBLY}/${DE_RES_FILE}" \
--pval 				  	      "${PVAL}" \
--thres_alignedPct		  "${PERC_ALN}"

# LAST vs BLAST (w/ 0 PERC_ALN)
Rscript evaluate_3.x_mudcrabData.R \
--out_dir 				      "${DIR_OUT}/last_vs_assembly.blast" \
--file_contig2estg  	  "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
--file_blastAlignment   "${FILE_BLAST}" \
--file_lastRes 			    "${DIR_DE_LAST}/${DE_RES_FILE}" \
--file_assemblyRes 		  "results/DE_analysis/mudcrab_revisited/${SITE}/assembly.blast/${DE_RES_FILE}" \
--pval 				  	      "${PVAL}" \
--thres_alignedPct		  0
