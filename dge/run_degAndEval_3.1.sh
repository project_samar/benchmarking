#! /bin/bash

# List of Variables
DIR_SIM_DATA="data/fruit_fly_sim_results/20mil_1txperGene_fpkmemtab6584_protCoding_rep3_nodup"
DIR_SIM_REF="data/fruit_fly_data"
DIR_DE_LAST="results/DE_analysis/last"
DIR_DE_RSEM="results/DE_analysis/rsem"

# List of Constants
FACTORS='["Group"]'
DESIGN="Group"
SAMPLE_INFO='{"sample_1": ["control"], "sample_2": ["control"], "sample_3": ["control"], "sample_4": ["treated"], "sample_5": ["treated"], "sample_6": ["treated"]}'
DE_RES_FILE="Test_result-Group_treated_vs_control.csv"

# DE Analysis - LAST
Rscript modules/DEAnalysis/last2deseq.R \
--in_dir       "${DIR_SIM_DATA}/our_gene_level_trained" \
--out_dir      "${DIR_DE_LAST}" \
--factors      "${FACTORS}" \
--design       "${DESIGN}" \
--sample_info  "${SAMPLE_INFO}"

# DE Analysis - RSEM
Rscript modules/DEAnalysis/rsem2deseq.R \
--in_dir       "${DIR_SIM_DATA}/bowtie2_rsem" \
--file_tx2gene "${DIR_SIM_REF}/genes2transcripts" \
--out_dir      "${DIR_DE_RSEM}" \
--factors      "${FACTORS}" \
--design       "${DESIGN}" \
--sample_info  "${SAMPLE_INFO}"

# Evaluation
Rscript evaluate_3.1_withReference.R \
--out_dir       "results/DE_evaluation/3.1 - With Reference" \
--g2t           "${DIR_SIM_REF}/genes2transcripts" \
--t2p           "${DIR_SIM_REF}/transcripts2proteins" \
--sim_rep_info  "${DIR_SIM_DATA}/ground_truth/sim_rep_info.txt" \
--sim_tx_info   "${DIR_SIM_DATA}/ground_truth/sim_tx_info.txt" \
--file_lastRes  "${DIR_DE_LAST}/${DE_RES_FILE}" \
--file_rsemRes  "${DIR_DE_RSEM}/${DE_RES_FILE}"