#! /bin/bash

# List of Variables
SITE="BAT"
DIR_COUNTS="data/mudcrab_revisited/${SITE}"
DIR_DE_LAST="results/DE_analysis/mudcrab_revisited/${SITE}/last"
DIR_DE_ASSEMBLY="results/DE_analysis/mudcrab_revisited/${SITE}/assembly_percAln0.5"
DIR_DE_LAST_BL80M1="${DIR_DE_LAST}_BL80_m1"
DIR_DE_ASSEMBLY_SALMON="${DIR_DE_ASSEMBLY}_salmon"
PERC_ALN=0.5
PVAL=0.01

# List of Constants
FACTORS='["Group"]'
DESIGN="Group"
DE_RES_FILE="Test_result-Group_treated_vs_control.csv"
SAMPLE_INFO='{"sample_1": ["control"], "sample_2": ["control"], "sample_3": ["control"], "sample_4": ["treated"], "sample_5": ["treated"], "sample_6": ["treated"]}'

# DE Analysis - LAST
Rscript modules/DEAnalysis/last2deseq.R \
--in_dir       "${DIR_COUNTS}/last" \
--out_dir      "${DIR_DE_LAST}" \
--factors      "${FACTORS}" \
--design       "${DESIGN}" \
--sample_info  "${SAMPLE_INFO}"

# DE Analysis - LAST new filter DEG (available for BAT only)
Rscript modules/DEAnalysis/last2deseq.R \
--in_dir       "${DIR_COUNTS}/last_BL80_m1" \
--out_dir      "${DIR_DE_LAST_BL80M1}" \
--factors      "${FACTORS}" \
--design       "${DESIGN}" \
--sample_info  "${SAMPLE_INFO}"

# DE Analysis - Assembly
Rscript modules/DEAnalysis/assembly2deseq.R \
--in_dir               "${DIR_COUNTS}/assembly" \
--file_contig2estg     "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
--file_dammitAlignment "${DIR_COUNTS}/assembly/Trinity.fasta.x.sprot_arthropoda_cluster100.fasta.crbl.csv" \
--file_dammitNamemap   "${DIR_COUNTS}/assembly/Trinity.fasta.dammit.namemap.csv" \
--count_method         "rsem" \
--out_dir              "${DIR_DE_ASSEMBLY}" \
--thres_alignedPct     "${PERC_ALN}" \
--factors              "${FACTORS}" \
--design               "${DESIGN}" \
--sample_info          "${SAMPLE_INFO}"

# DE Analysis - Assembly using Salmon DEG (available for BAT only)
Rscript modules/DEAnalysis/assembly2deseq.R \
--in_dir               "${DIR_COUNTS}/assembly_salmon" \
--file_contig2estg     "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
--file_dammitAlignment "${DIR_COUNTS}/assembly/Trinity.fasta.x.sprot_arthropoda_cluster100.fasta.crbl.csv" \
--file_dammitNamemap   "${DIR_COUNTS}/assembly/Trinity.fasta.dammit.namemap.csv" \
--count_method         "salmon" \
--out_dir              "${DIR_DE_ASSEMBLY_SALMON}" \
--thres_alignedPct     "${PERC_ALN}" \
--factors              "${FACTORS}" \
--design               "${DESIGN}" \
--sample_info          "${SAMPLE_INFO}"


# Evaluation output
DIR_OUT="results/DE_evaluation/3.x - Mudcrabs/${SITE}_setDiffEval"

# LAST vs Assembly
Rscript evaluate_3.x_mudcrabData.R \
--out_dir 				      "${DIR_OUT}/last_vs_assembly_percAln0.5" \
--file_dammitAlignment  "${DIR_COUNTS}/assembly/Trinity.fasta.x.sprot_arthropoda_cluster100.fasta.crbl.csv" \
--file_dammitNamemap 	  "${DIR_COUNTS}/assembly/Trinity.fasta.dammit.namemap.csv" \
--file_contig2estg  	  "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
--file_lastRes 			    "${DIR_DE_LAST}/${DE_RES_FILE}" \
--file_assemblyRes 		  "${DIR_DE_ASSEMBLY}/${DE_RES_FILE}" \
--pval 				  	      "${PVAL}" \
--thres_alignedPct		  "${PERC_ALN}"

# # Execute following FOR BAT ONLY
# LAST_BL80_m1 vs Assembly   
Rscript evaluate_3.x_mudcrabData.R \
--out_dir 				     "${DIR_OUT}/last_BL80_m1_vs_assembly_percAln0.5" \
--file_dammitAlignment "${DIR_COUNTS}/assembly/Trinity.fasta.x.sprot_arthropoda_cluster100.fasta.crbl.csv" \
--file_dammitNamemap 	 "${DIR_COUNTS}/assembly/Trinity.fasta.dammit.namemap.csv" \
--file_contig2estg  	 "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
--file_lastRes 			   "${DIR_DE_LAST_BL80M1}/${DE_RES_FILE}" \
--file_assemblyRes 		 "${DIR_DE_ASSEMBLY}/${DE_RES_FILE}" \
--pval 					       "${PVAL}" \
--thres_alignedPct		 "${PERC_ALN}"

# # LAST vs Assembly Salmon
Rscript evaluate_3.x_mudcrabData.R \
--out_dir 				      "${DIR_OUT}/last_vs_assembly_percAln0.5_salmon" \
--file_dammitAlignment  "${DIR_COUNTS}/assembly/Trinity.fasta.x.sprot_arthropoda_cluster100.fasta.crbl.csv" \
--file_dammitNamemap 	  "${DIR_COUNTS}/assembly/Trinity.fasta.dammit.namemap.csv" \
--file_contig2estg  	  "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
--file_lastRes 			    "${DIR_DE_LAST}/${DE_RES_FILE}" \
--file_assemblyRes 		  "${DIR_DE_ASSEMBLY_SALMON}/${DE_RES_FILE}" \
--pval 					        "${PVAL}" \
--thres_alignedPct		  "${PERC_ALN}"

# # LAST_BL80_m1 vs Assembly Salmon
Rscript evaluate_3.x_mudcrabData.R \
--out_dir 				      "${DIR_OUT}/last_BL80_m1_vs_assembly_percAln0.5_salmon" \
--file_dammitAlignment  "${DIR_COUNTS}/assembly/Trinity.fasta.x.sprot_arthropoda_cluster100.fasta.crbl.csv" \
--file_dammitNamemap 	  "${DIR_COUNTS}/assembly/Trinity.fasta.dammit.namemap.csv" \
--file_contig2estg  	  "${DIR_COUNTS}/assembly/Trinity.fasta.gene_trans_map" \
--file_lastRes 			    "${DIR_DE_LAST_BL80M1}/${DE_RES_FILE}" \
--file_assemblyRes 		  "${DIR_DE_ASSEMBLY_SALMON}/${DE_RES_FILE}" \
--pval 					        "${PVAL}" \
--thres_alignedPct		  "${PERC_ALN}"

