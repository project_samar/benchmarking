#! /bin/bash

# List of Variables
DIR_SIM_DATA="data/fruit_fly_sim_results/20mil_1txperGene_fpkmemtab6584_noref"
DIR_DE_LAST="results/DE_analysis/last_agam"
DIR_DE_ASSEM="results/DE_analysis/assembly_agam"
PERC_ALN=0

# List of Constants
FACTORS='["Group"]'
DESIGN="Group"
SAMPLE_INFO='{"sample_1": ["control"], "sample_2": ["control"], "sample_3": ["control"], "sample_4": ["treated"], "sample_5": ["treated"], "sample_6": ["treated"]}'
DE_RES_FILE="Test_result-Group_treated_vs_control.csv"


# DE Analysis - LAST
Rscript modules/DEAnalysis/last2deseq.R \
--in_dir       "${DIR_SIM_DATA}/our_agambiae_trained_disjoint0.1" \
--out_dir      "${DIR_DE_LAST}" \
--factors      "${FACTORS}" \
--design       "${DESIGN}" \
--sample_info  "${SAMPLE_INFO}"

# DE Analysis - Assembly
Rscript modules/DEAnalysis/assembly2deseq.R \
--in_dir                "${DIR_SIM_DATA}/assembly" \
--file_contig2estg      "${DIR_SIM_DATA}/assembly/Trinity.fasta.gene_trans_map" \
--file_dammitAlignment  "${DIR_SIM_DATA}/assembly/Trinity.fasta.x.A.gambiae.fasta.crbl.bestaln.csv" \
--file_dammitNamemap    "${DIR_SIM_DATA}/assembly/Trinity.fasta.dammit.namemap.csv" \
--thres_alignedPct      "${PERC_ALN}" \
--out_dir               "${DIR_DE_ASSEM}" \
--factors               "${FACTORS}" \
--design                "${DESIGN}" \
--sample_info           "${SAMPLE_INFO}"


# Evaluation
DIR_SIM_REF="data/fruit_fly_data"
DIR_SIM_GROUNDTRUTH="data/fruit_fly_sim_results/20mil_1txperGene_fpkmemtab6584_protCoding_rep3_nodup/ground_truth"

Rscript evaluate_3.2_withoutReference.R \
--out_dir              "results/DE_evaluation/3.2 - No Reference" \
--g2t                  "${DIR_SIM_REF}/genes2transcripts" \
--t2p                  "${DIR_SIM_REF}/transcripts2proteins" \
--file_all_tx_ref      "${DIR_SIM_REF}/transcripts_in_reference" \
--file_agam2FlyProt    "${DIR_SIM_REF}/A.gambiae-D.melanogaster" \
--file_flyProt2g       "${DIR_SIM_REF}/uniprotkb2flybase(allseqpergene)" \
--sim_rep_info         "${DIR_SIM_GROUNDTRUTH}/sim_rep_info.txt" \
--sim_tx_info          "${DIR_SIM_GROUNDTRUTH}/sim_tx_info.txt" \
--file_contig2estg     "${DIR_SIM_DATA}/assembly_agambiae/Trinity.fasta.gene_trans_map" \
--file_dammitAlignment "${DIR_SIM_DATA}/assembly_agambiae/Trinity.fasta.x.A.gambiae.fasta.crbl.bestaln.csv" \
--file_dammitNamemap   "${DIR_SIM_DATA}/assembly_agambiae/Trinity.fasta.dammit.namemap.csv" \
--file_lastRes         "${DIR_DE_LAST}/${DE_RES_FILE}" \
--file_assemblyRes     "${DIR_DE_ASSEM}/${DE_RES_FILE}" \
--thres_alignedPct     ${PERC_ALN}