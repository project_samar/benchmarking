This README shows how to reproduce the results in the manuscript that describes [SAMAR](https://bitbucket.org/project_samar/samar).

# 1. Software requirement
Conda (tested on v. 4.8.4) and Snakemake (tested on v.5.30.1) are required. All other dependencies are handled by Snakemake.
## 1.1. Install Conda 
Download Miniconda3  installer for Linux from  [here](https://docs.conda.io/en/latest/miniconda.html#linux-installers).
Installation instructions are [here](https://conda.io/projects/conda/en/latest/user-guide/install/linux.html).
Once installation is complete, you can test your Miniconda installation by running:
```
$ conda list
```

## 1.2. Install Snakemake
Snakemake recommends installation via Conda/Mamba:
```
$ conda install -c conda-forge mamba
$ mamba create -c conda-forge -c bioconda -n snakemake snakemake
```
This creates an isolated enviroment containing the latest Snakemake. To activate it:
```
$ conda activate snakemake
```
To test snakemake installation 
```
$ snakemake --help
```

## 1.3. Download/clone this repository:
```
git clone https://bitbucket.org/project_samar/benchmarking.git
cd benchmarking
```

## 1.4. Download SAMAR 
Download rom the online [repository](https://bitbucket.org/project_samar/samar), or using the command line:
```
git clone https://bitbucket.org/project_samar/samar.git
```

# 2. Data
Download and unzip the necessary data from [here](https://drive.google.com/file/d/1DP2H5qLJ4cnFU1uf1-iZBQcgXd7Xxzoq/view?usp=sharing). 
The contents of the folder are described in *file_inventory.md* inside the data folder.

# 3. Reproducing the results on simulated read dataset
Below we assume the following directory structure:
```
samar
+---workflow
+---config
+---..and so on..
benchmarking
+---Snakefiles
+---config
+---..and so on..
data
+---dro_me_ref1pg.fa
+---A.gambiae.fasta
+---..and so on..
```

## 3.1. Simulate the RNA-seq reads
With the snakemake conda environment activated and from immediately inside *benchmarking* folder:
```
snakemake --snakefile Snakefiles/Snakefile_simulation --configfile config/config_file.simulation.yaml --directory ../data --use-conda --cores all
```
If using a different directory structure, set the *--directory* argument or the paths in *config_file.simulation.yaml* accordingly.

The simulated RNA-seq reads will be placed under the newly created *sim_reads_dir* inside the *data* folder.

The read dataset that was used for our study can be downloaded from [here](https://drive.google.com/file/d/1ciHGJYSuJDb4cfzItX411WC-1-EroMip/view?usp=sharing). (Warning: Huge file) 

## 3.2. Run the assembly-based pipeline (i.e. Trinity-Bowtie2-RSEM-tximport-DEseq2 pipeline)
```
snakemake --snakefile Snakefiles/Snakefile_assembly --configfile config/config_file.simulation.yaml --directory ../data --use-conda --conda-frotend mamba --cores all
```
You will find all output data inside the folder *assembly_sim* inside the *data* folder.  Inside *assembly_sim*, the folder *trinity_out_dir* contains the assembly, 
*bowtie2_trinity_alignments* contains the alignments, *rsem_trinity_counts* contains the count data, 
and folder *DEanalysis* contains the DE analysis results.


## 3.3 Run SAMAR
From the top-level directory of SAMAR, run:
```
snakemake --configfile ../benchmarking/config/config_file.simulation.yaml --directory ../data --use-conda --conda-frontend mamba --cores all
```
The results will be inside the folder *samar* inside *data*. There, folder *last_alignments* contains alignments, 
folder *counts* contain the per-sample per-gene counts, folder *DEanalysis* contains the result of DE analysis.
 
# 4. Reproducing the results on the real read dataset
Download the datasets ArrayExpress E-MTAB-8090 ERR3393437--42.
Below, we assume the following directory structure. 
```
samar
benchmarking
e_mtab_8090
+--ERR3393437_1.fastq
+--ERR3393437_2.fastq
+--ERR3393438_1.fastq
+--ERR3393438_2.fastq
+-- and so on.
```
If this not the case, you can modify the paths in *config/config_file.real.yaml* accordingly.

# 4.1. Running the assembly-based pipeline
From the top-level directory of *benchmarking*, run:
```
snakemake --snakefile Snakefiles/Snakefile_assembly --configfile config/config_file.real.yaml --directory ../e_mtab_8090 --use-conda --conda-frotend mamba --cores all

```
# 4.2. Running SAMAR
From the top-level directory of *samar*, run:
```
snakemake --configfile ../benchmarking/config/config_file.real.yaml --directory ../data --use-conda --conda-frontend mamba --cores all
```
