Rscript simulation.R \
--in_file_referenceFa "//data-refless/simulation_fruitfly/references/transcripts_ensembl_uniprot.fa" \
--in_file_fpkm "//data-refless/simulation_fruitfly/references/FPKM_emtab6584" \
--in_file_genes2trans"//data-refless/simulation_fruitfly/references/genes2transcripts" \
--out_file_faSubset "//data-refless/simulation_fruitfly/references/transcripts (nonzero reads).fa" \
--out_path "//data-refless/simulation_fruitfly/reads_10mil_1txpg_protCoding.rep3"